#include <iostream>

class Vector
{
public:

	void ShowModuleVector()
	{
		std::cout << "Module = " << Module << std::endl;
	}

	Vector(): x(1),y(2),z(3)
	{}

	void ShowVector()
	{
		std::cout << x << " " << y << " " << z << std::endl;
	}
private:
	double x;
	double y;
	double z;
	double Module = sqrt(x * x + y * y + z * z);
};

int main()
{
	Vector temp;
	temp.ShowVector();
	temp.ShowModuleVector();
}